# Software architecture

This section documents the software architecture of the platform.

## Introduction

The whole platform is built with RESTful guidelines in mind, because REST makes it possible to access and manage data in an easily understandable way. For further reference, consider the [wikipedia page about REST](https://en.wikipedia.org/wiki/Representational_state_transfer).

All data entities like machines, users and other resources are represented as REST resources. Resources, in the context of REST, are URL endpoints like `https://api.thecore.sdu.dk/v1/users`. Each of these resources correspond to an entity model in the database. To be able to work with the data in a meaningful way, a **CRUD** (**c**reate, **r**ead, **u**pdate, **d**elete) interface needs to be implemented. This can be done by implementing REST on top of the HTTP application layer of the OSI model. In this case, actions to a resource are often mapped to an HTTP method:

- `GET` requests **read** one or more entities of a resource
- `POST` requests **create** one or more entities of a resource
- `PATCH` requests **update** one or more entities of a resource
- `DELETE` requests **delete** one or more entities of a resource

So, for example, to read a single user, the following request can be made:

`GET https://api.thecore.sdu.dk/v1/users/:userId`

In contrast, if all users should be read, the following request can be made:

`GET https://api.thecore.sdu.dk/v1/users`

## Drawing

This drawing does not involve communication protocols, because it mainly focuses on the different vertical layers.

The topmost layer describes the client layer. Users interact with it and use it to manage data. However this layer also includes the IoT nodes, because they also consume the services of the underlying service layer. The version control system is special, because it contains its own client, service and data layer.

The middle layer is, as briefly mentioned before, the service layer. It provides ways to access to the underlying data layer. This access management includes the usage of special protocols, like REST, to implement authentification (_Who am I?_) and authorization (_What may I do?_) to the underlying data. In this context the identity provider is special, because it provides the authentification service and the underlying data for it. In theory it also provides an interface, but this is only a login form or a consent screen and is therefore neglected.

The bottommost layer is the data layer, which is only in charge of data storage and state management. It ensures, that data is persisted between servers and deployments.

![Software architecture](../images/architecture-software.png)

