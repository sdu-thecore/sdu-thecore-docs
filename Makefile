TAG ?= dev

.PHONY: dev build run
dev:
	mkdocs serve

build:
	docker build -t sdu-labcloud/documentation:$(TAG) .

run:
	docker run --rm -p 8000:80 sdu-labcloud/documentation:$(TAG)
