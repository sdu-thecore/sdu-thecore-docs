# Welcome

Popular topics

- [How to 3D print](users/3d-printing/3d-printing.md)

This page documents the **LabCloud** manufacturing platform. The documentation is grouped into different sections based on the user groups:

- [Users](users/home.md): You want to understand how to use machines and 3D-printers.
- [Admins](admins/home.md): You want to connect machines to the platform and manage user permissions.
- [Developers](developers/home.md): You want to understand and improve the platform.

## License

This project is licensed under the terms of the [MIT license](https://mit-license.org).
