# Smart node

The smart node can be used for a variety of applications. Currently two scenarios are supported: `device-mangement` and `inventory-management`.

## Setup and installation

1. Prepare the hardware. To provision the Raspberry Pi you will need:

   - A Raspberry Pi 3B or 3B+
   - A 5V 2.5A micro USB power supply
   - An 8GB micro SD card
   - A 1GB FAT32 formatted USB drive
   - A computer with a micro SD card slot or a computer with an appropiate adapter

1. You will need also need a program to flash an operating system `.img` file to an SD card. A good program for this is [balenaEtcher](https://www.balena.io/etcher/). Download the appropiate version and start the program.

1. Download the latest firmware image from [this](https://gitlab.com/sdu-labcloud/labcloud-node-linux-image-generation/-/jobs/artifacts/master/download?job=raspbian) link and unpack it.

1. Insert the micro SD card into your computer. The software should automatically detect your micro SD card.

1. Select the recently extracted `.img` file and click the `Flash` button.

1. Once the image is successfully flashed, eject the micro SD card from your computer and insert it into your Raspberry Pi.

1. Power up the Raspberry Pi and wait for the green LED to blink steadily. This may take a while.

1. In the meantime insert the USB drive into your computer and create two files in the root of it:

```ini
# Save this file as "install.ini".
# Insert your WiFi network name.
SSID=WiFiNetwork
# Insert your WiFi network password or leave blank for open networks.
PSK=WiFiPassword
# Set it to "WPA-PSK" if you have a password or "None" if you have an open network.
KEY_MGMT=WPA-PSK
# A URL to the installation script for the software on the node.
SCRIPT_URL=https://gitlab.com/sdu-labcloud/labcloud-node-linux/raw/master/scripts/install-application.sh
```

```ini
# Save this file as "environment.ini".
POLL_INTERVAL=500
MOCK_HARDWARE=false
API_URL=https://api.thecore.sdu.dk
HUB_URL=https://hub.thecore.sdu.dk
```

1. Eject the USB drive and insert it into the Raspberry Pi. Now, wait for the green LED to blink steadily again. You can now remove the USB.

1. Now wait for the Raspberry Pi to register itself against the API and have a look at the user interface. This might take a while as the device has to install the program and all necessary dependencies first. Once ready, a new node will show up in the interface.
