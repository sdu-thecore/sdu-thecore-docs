# Documentation

This repository is a collection of documentation for the manufacturing platform.

## Installation

Make sure to have `python3` installed. We recommend [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) for managing your virtual environment. You can the install the dependencies by running:

```shell
$ pip install -r requirements.txt
```

Once you have the dependencies installed, you can start the development server:

```
$ mkdocs serve
```

Now point your browser to [http://localhost:8000](http://localhost:8000).

## Deployment

This repository creates tagged Docker images, that can be deployed to a Docker host or Kubernetes. To launch the Docker container, you would typically run:

```shell
$ docker run -d -p 8080:80 registry.gitlab.com/sdu-labcloud/documentation
```

## License

This project is licensed under the terms of the [MIT license](https://mit-license.org/).
