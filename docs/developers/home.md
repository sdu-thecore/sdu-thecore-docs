# Overview

The developer documentation contains the following chapters:

- [Contribution guidelines](contribution-guidelines.md)
- [Software architecture](software-architecture.md)
- [Hardware](hardware.md)
- [Platform deployment](platform-deployment.md)
- [Security assessment](security-assessment.md)
