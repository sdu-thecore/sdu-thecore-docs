# Platform deployment

This section covers the deployment of the server-side components of the platform via Docker containers.

## Requirements

To deploy the platform, a server, either virtual or physical, is needed that fulfills the following requirements:

- SSH access
- Ubuntu 18.04 LTS
- 4 GB RAM
- 2 vCPUs
- 100 GB SSD persistent disk
- Public IP address

Additionally, the following is required:

- DNS A record for `thecore.sdu.dk` pointing to the public IP address of the server
  - Used for the user-facing web application
- DNS A wildcard record for `*.thecore.sdu.dk` pointing to the public IP address of the server
  - Used for the REST API at `api.thecore.sdu.dk`
  - User for the documentation at `docs.thecore.sdu.dk`
- Public IP Port 22 TCP is forwared to private IP Port 22 TCP
  - Used for SSH connections to the server
- Public IP Port 80 TCP is forwared to private IP Port 80 TCP
  - Used to redirect HTTP traffic to HTTPS
- Public IP Port 443 TCP is forwared to private IP Port 443 TCP
  - Used to serve the applications via an SSL-terminating reverse proxy
- Credentials to the email account `thecore@sdu.dk`
  - Used for email notifications

## Preparation

The platform is delivered via Docker containers and will also be deployed on a host with a running Docker daemon. Therefore Docker has to be installed as described **[here](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository)**.

After installing the Docker daemon, you need to add your user to the `docker` group:

```bash
$ sudo usermod -aG docker $(whoami)
```

After doing this, log out from the terminal and log in again. Usually, this can be done by just typing `exit` and then reestabling the SSH session. Now, test Docker by running:

```bash
$ docker ps -a
```

As we are installing multiple applications on the same Docker host, which should all use port 80 and port 443, we need a reverse proxy. For this we will use a nice Docker container, which automates the creation of virtual hosts:

```bash
$ docker run -d -p 80:80 -p 443:443 \
    --name proxy-nginx \
    --restart always \
    -v /opt/proxy-nginx/certs:/etc/nginx/certs:ro \
    -v /etc/nginx/vhost.d \
    -v /usr/share/nginx/html \
    -v /var/run/docker.sock:/tmp/docker.sock:ro \
    --label com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy \
    jwilder/nginx-proxy
```

Because we want to have transport layer security (TLS), we need to set up an additional container that can automatically request cross-signed certificates by a valid certificate authority (CA). This is can done, free of charge, by help of [Let’s Encrypt](https://letsencrypt.org) and another small docker container:

```bash
$ docker run -d \
    --name proxy-nginx-letsencrypt \
    --restart always \
    -v /opt/proxy-nginx/certs:/etc/nginx/certs:rw \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    --volumes-from proxy-nginx \
    jrcs/letsencrypt-nginx-proxy-companion
```

Now test your proxy deployment by running a test container:

```bash
$ docker run -d \
    --name app-test \
    -e "VIRTUAL_HOST=test.thecore.sdu.dk" \
    -e "LETSENCRYPT_HOST=test.thecore.sdu.dk" \
    -e "LETSENCRYPT_EMAIL=thecore@sdu.dk" \
    crccheck/hello-world
```

Wait for one to five minutes in order for the certificate to be issued. Afterwards run the following:

```bash
$ curl -s https://test.thecore.sdu.dk | grep "Hello World" > /dev/null && echo "Test succeeded!" || echo "Test failed!"
```

If the test succeeded, it will display `Test succeeded!`. Otherwise, it will output `Test failed!`.

After a successful test, stop and remove the test application:

```bash
$ docker stop app-test && docker rm app-test
```

