# Node setup

This section goes through the use and setup for the nodes for the workshop.

For the use of the machinery in the workshop, a key system is needed. This is handled through a booking system which uses the RFID chip in the studentcards.
The nodes are the card readers applied to the different machines and tools, and function as a connection between the server and the real world.

The node system uses two different types: a smart node, and a simple node.
The smart nodes are used to control the laser cutter and 3D-printers, since these could use the USB connections for further development.
The simple nodes are used to control the drills, hack-saw, foam slicers, and the sander, since these just need a direct power control through a relay.

## RFID Card Readers

The Core project uses the Chilli B1 RFID card readers. These need to be soldered and configured before use. Go to [Eccel's website](https://eccel.co.uk/support/downloads/#13-56mhz-products) and download the **B1 Stand-alone Configurator for Windows.** Look under the _Chilli UART B1_ section, item 3 at the time of writing.

Connect the RFID reader and launch the executable.

![B1 Stand-alone Configurator](../images/node-setup-standalone-configurator.png)

Choose the COM port corresponding to the RFID reader and click `Connect`. If you have multiple devices connected, you may have to try a few different ports. In this case, it was COM14.

![B1 Stand-alone Configurator COM ports](../images/node-setup-standalone-configurator-com.png)

The bar below `Connect` should now be green and the device is connected. Configure the settings as seen below.

![B1 Stand-alone Configurator Settings](../images/node-setup-standalone-configurator-settings.png)

After you have configured the settings correctly, click `Save configuration`.

![B1 Stand-alone Configurator](../images/node-setup-standalone-configurator-save.png)

Now click `Start polling`.

![B1 Stand-alone Configurator](../images/node-setup-standalone-configurator-poll.png)

The RFID card reader is now configured correctly. Click on `Disconnect` and the device is ready to integrate into a node.

## Smart Nodes

The smart nodes uses a Raspberry Pi 3B+ for the computing power, extensive documentation, and expandability with several USB ports.

### OS Setup

The custom Pi image can be downloaded by clicking [this link](https://gitlab.com/sdu-labcloud/labcloud-node-linux-image-generation/-/jobs/artifacts/master/raw/raspbian-buster-lite-usb-provisioning-lite.img?job=raspbian).

If a format of the system is needed, [SD Card Formatter](https://www.sdcard.org/downloads/formatter_4/) is recommended.

### Setting Up Installation Script.

For the WiFi setup, create an `install.ini` file with the following content

```ini
WIFI_SSID     = <SSID>
WIFI_SCAN     = <HIDDEN_NETWORK>
WIFI_PSK      = <PSK>
WIFI_KEY_MGMT = <KEY_MGMT>
SCRIPT_URL    = <INIT_URL>
```

Where the **WIFI_SSID** is the name of the network, and the **WIFI_PSK** is the password. The **WIFI_KEY_MGMT** is the type of network security. If the security is **WPA/WPA2**, it should be filled out with `WPA-PSK` and if it is an open network it should be `NONE` with the **WIFI_PSK** left empty.
If the network is hidden, the **WIFI_SCAN** should be `1`, and should otherwise be `0`.

The **SCRIPT_URL** is the link to the Bash script for the installation of the main program, which can be found [here](https://gitlab.com/sdu-thecore/sdu-thecore-node-linux/blob/feature/git-update/src/scripts/smart-node-init.sh). Link the raw code from the repository.

When the file has been made, put it on a USB drive in the root folder and plug the drive into the Raspberry Pi, and turn on the Pi.

### Component List

To build a Raspberry Pi, the following items are needed

### Wiring

**Raspberry Pi 3B Pinout**

![Raspberry Pi 3B Pinout](../images/smart-node/raspberry-pi-3b-pinout.png)

(picture found at https://indibit.de/raspberry-pi-die-gpio-schnittstelle-grundlagenbelegung/)

**Power Supply:** The live and neutral wires from the main's supply goes into the L and N ports on the power supply. The 5V pins to the Relay and Raspberry Pi connects to the V+ port on the power supply, with V- connected to the respective parts' ground pins.

**RFID Reader:** The power cable from the RFID reader goes to the 1st pin (3V3 output) on the pi. The RX/TX cables goes to their respective pair on the raspberry pi's 8th and 10th pins.
The ground cable goes to one of the Pi's GND pins: 6, 9, 14, 20, 25, 30, 34, 39.

**Relay:** The relay should be connected to the Pi's 11th pin (GPIO17) via the input terminal `in`. The High/Low level trigger jumper should be set to high level triggers.
The Vin port is the 5V input that connects to the power supply.

**LED:** The Din input from the LED connects to the 12th pin (GPIO18). The + (5V) should not be put on a 5V output, but instead use one of the Pi's 3V3 outputs, pin 1 or pin 17.and - (GND) inputs connects to the powersupply in their respective slots.

![Wiring Diagram](../images/smart-node/smart-node-wiring.jpg)

## Simple Nodes

The simple nodes use the Sonoff Basic, which is powered by an ESP8266 microcontroller. The board has an integrated power supply and relay, which makes it ideal for this application.

### Programming

The simple node uses a custom Micropython firmware. A Python script is used to upload the firmware to the node. Since the Sonoff Basic does not have a programming port, header pins must be soldered onto the board. Solder a 5 pin header to the open ports next to the button. See the picture below for reference on which ports to solder to.

![Sonoff Programming pins](../images/simple-node/simple-node-programming-pinout.png)

A USB to UART adapter is needed, such as the [Sparkfun Electronics FT232RL](https://www.sparkfun.com/products/12731), but any USB to Serial board will work. **It is important that the adapter can provide 3v3 volts, as the ESP8266 on the board will be damaged by 5V.**

The wires should be connected as labelled:

![Wiring for programming](../images/simple-node/simple-node-programming-wiring.png)

_You may have to switch the RX and TX connections, as they are not consistently labelled across devices._

In order to program the device, Python must be installed on the PC. Both Python 2.7 and 3.4+ will work. Once installed, use [pip](https://packaging.python.org/tutorials/installing-packages/) to install esptool. Open a terminal/command prompt window and type

```bash
python pip install esptool
```

Once you have successfully installed esptool, download the latest version of the firmware from the [Gitlab repo](https://gitlab.com/Clapfire/simple-node). Once downloaded, extract the .bin file from the zip file using a program like [7-Zip](https://www.7-zip.org/). Extract it to your desktop. Open a terminal/command prompt window and type

```bash
WINDOWS:
cd C:\Users\USERNAME\Desktop

MAC OS X:
cd ~/Desktop
```

Now plug in the USB adapter, while keeping the button pressed on the Sonoff Basic. Then type

```bash
python -m esptool -b 921600 erase_flash
```

Once the operation is complete, unplug the USB adapter, and plug it back in, while keeping the button pressed. Then type:

```bash
python -m esptool -b 921600 write_flash 0x0 firmware-combined.bin
```

If there are no errors, the device will be ready to integrate into a node.

### Wiring

The pinout of the Sonoff basic is shown on the image below:

![Sonoff Basic Pinout](../images/simple-node/simple-node-pinout.png)

The RFID card reader and the status LED will both be connected to the 3V3 and GND pins. The RFID card reader will be connected to the RX and TX pins (TX->RX, RX->TX) and the LED will be connected to the GPIO14 pin. The live and neutral from the mains supply go to the L and N input.

A basic wiring diagram is shown below:

![Simple node wiring diagram](../images/simple-node/simple-node-diagram.png)

### Setting up wifi

When the node is first booted up, if it cannot connect to a wifi network, it will start an access point. The SSID will be "simple-node-XXXX", where the last 4 characters are unique to each node.
Connect to this access point and open a socket to 192.168.4.1:80. The node will expect two tranmissions: The SSID and the password.

A sample Python script that performs this action:

```python
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('192.168.4.1', 80))

s.send(b'SSID')
s.send(b'PASSWORD')
s.close()
```

The node will attempt to connect using these credentials. If it is succesful, it will disable the access point and continue the program. If it is unsuccesful, it will await a new socket.

### Sessions

When a node is connected to the internet, it will create a node entry in the database. You will be able to identify it based on the serial number, mac address and operating system. Currently, the two types are Micropython and Linux, which correspond to the Sonoff Basic and Raspberry Pi respectively. After a node has been created in the system, it will wait for a session to be authorized. This is done through the Nodes pages at thecore.sdu.dk. This page will only be available if you are authorized to manage nodes.

### Identifying Nodes

Every node can be indentified using the indicator light, as long as it is connected to the internet. To identify which node an entry corresponds to, use the "Blink LED" option. When this option is enabled, the indicator LED on the node will blink orange.
