# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.10.2] - 2021-04-06

### Fixed

- Correct information regarding the smart node setup

## [0.10.1] - 2020-11-27

### Fixed

- Typo on homepage

## [0.10.0] - 2020-11-27

### Added

- Documentation on how to 3D-print

## [0.9.2] - 2020-09-21

### Added

- Document how users can submit feedback

## [0.9.1] - 2020-09-21

### Added

- Document user login

## [0.9.0] - 2020-09-18

### Changed

- Use `mkdocs` as documentation framework

## [0.8.3] - 2019-05-10

### Changed

- Rebrand to `LabCloud`

## [0.8.2] - 2019-05-09

### Changed

- Updated documentation for the smart node with the new provisioning process

## [0.8.1] - 2019-01-21

### Fixed

- Change case of image extension of simple-node-diagram

## [0.8.0] - 2019-01-10

### Added

- Bill of Material for nodes
- Link to Google Drive for node cases

## [0.7.0] - 2019-01-03

### Added

- Improved smart and simple node documentation
- Wiring diagrams for smart and simple node

## [0.6.2] - 2018-11-22

### Added

- Documentation for node RFID setup

### Fixed

- Tagging now fails with an error code of `1` if the git tag already exists (#28)

## [0.6.1] - 2018-10-12

### Fixed

- Typo in `.gitlab-ci.yml`, which caused post-deployment check to fail

## [0.6.0] - 2018-10-12

### Added

- Connect deployment to environment in GitLab
- Set up post-deployment test
- Automatic tagging will check version in `package.json` and `package-lock.json`

### Fixed

- Improved platform deployment documentation

## [0.5.0] - 2018-10-09

### Added

- Documentation for Docker installation
- Documentation for reverse proxy setup
- Automatic deployment to docker host

### Fixed

- Version in `package.json` and `package-lock.json`

## [0.4.0] - 2018-10-08

### Changed

- Documentation of platform deployment requirements
- Reorganisation of all documentation

### Added

- Contribution guideline via [CONTRIBUTING.md](./CONTRIBUTING.md)
- Documentation for usage of Docker image (see [Dockerfile](./Dockerfile) for reference)
- Documentation categories
  - User documentation
  - Admin documentation
  - Developer documentation

## [0.3.1] - 2018-10-07

### Fixed

- Dependency upgrade check uses `david` instead of `npm-check-updates`
- Version in `package.json` and `package-lock.json`

## [0.3.0] - 2018-10-06

### Changed

- Use `npm-check-updates` instead of `npm-check-updates-lite`
- Development server uses port 8080 now

## [0.2.0] - 2018-10-06

### Fixed

- Improved platform deployment documentation

## [0.5.0] - 2018-10-09

### Added

- Documentation for Docker installation
- Documentation for reverse proxy setup
- Automatic deployment to docker host

### Fixed

- Version in `package.json` and `package-lock.json`

## [0.4.0] - 2018-10-08

### Changed

- Documentation of platform deployment requirements
- Reorganisation of all documentation

### Added

- Contribution guideline via [CONTRIBUTING.md](./CONTRIBUTING.md)
- Documentation for usage of Docker image (see [Dockerfile](./Dockerfile) for reference)
- Documentation categories
  - User documentation
  - Admin documentation
  - Developer documentation

## [0.3.1] - 2018-10-07

### Fixed

- Dependency upgrade check uses `david` instead of `npm-check-updates`
- Version in `package.json` and `package-lock.json`

## [0.3.0] - 2018-10-06

### Changed

- Use `npm-check-updates` instead of `npm-check-updates-lite`
- Development server uses port 8080 now

## [0.2.0] - 2018-10-06

### Added

- Documentation for group naming conventions
- Documentation for active third-party services
- Documentation for software architecture and required resources
- Documentation for node installation and setup
- Dockerfile for creation of Docker image
- Automatic tagging via [CHANGELOG.md](./CHANGELOG.md)

## [0.1.0] - 2018-09-14

### Added

- Documentation for usage of [Git](https://git-scm.com)
- Documentation for developer tooling
- Documentation for GitLab workflow
- GitLab pages
