### build environment ###
# base image
FROM python:3 AS build

# set workdir
WORKDIR /app

# copy requirements file
COPY ./requirements.txt .

# install dependencies
RUN pip install -r requirements.txt

# copy source files
COPY ./docs ./docs
COPY ./mkdocs.yml ./mkdocs.yml

# build static site
RUN mkdocs build --clean

### production environment ###
# base image
FROM nginx:mainline-alpine

# remove default virtual host configuration
# RUN mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.bak

# copy optimized virtual host configuration
# COPY config/nginx.conf /etc/nginx/conf.d/default.conf

# copy files to webroot
COPY --from=build /app/public /usr/share/nginx/html

# expose default nginx port
EXPOSE 80

# start nginx server
CMD ["nginx", "-g", "daemon off;"]
