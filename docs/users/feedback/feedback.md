# Submitting feedback

If the software does not work as intended or if you would like to request some additional features, you have the possibility to provide feedback. To do this, click on the **Feedback** menu item to the left.

![Feedback page](feedback-00.png)

The feedback page will automatically fill out your email and your name to allow us to contact you for further information. You can then proceed by selecting one of the following types:

- **Feature request**: You have an idea of how to improve the webpage or the usability.
- **Bug report**: Something is not working as intended.

![Feedback types](feedback-01.png)

For the title give a brief summary of the topic and provide a detailed description in the description field. If the feedback was successfully submitted, the page will show a brief summary of your feedback.

![Feedback summary](feedback-02.png)

You can then either submit more feedback or show the issue in GitLab, which will redirect you to GitLab, our internal management tool, where you can track the process of your feedback.

![Feedback in GitLab](feedback-03.png)
