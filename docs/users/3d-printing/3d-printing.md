# 3D-printing

This section explains how to 3D-print in the workshop.

## Exporting from CAD

Slicers accept a number of different file formats, the most common being STL (Standard Triangle Language). STL files can either be exported from a CAD program or downloaded from an online repository such as [Thingiverse](https://www.thingiverse.com/) or [GrabCAD](https://grabcad.com/).

![CAD export](../../images/3d-printing/cad-export.png)

## Flashforge Dreamer

![Flashforge Dreamer](../../images/3d-printing/flashforge-dreamer.jpg)

| Specifications          |               |
| ----------------------- | ------------- |
| Technology              | FDM / FFF     |
| Build volume            | 230x250x140mm |
| Layer height            | 0.1-0.3mm     |
| Maximum extrusion speed | 60mm/s        |
| Available materials     | PLA           |

### Slicer

For the Flashforge Dreamer use the FlashPrint slicer. You can download it <a href="https://www.flashforge.com/download-center" target="_blank">from the official website</a>.

![FlashPrint](../../images/3d-printing/flashprint.png)

### Before printing

The Flashforge Dreamer's bed is leveled by a three-point system, built using 3 screws under the build plate. Launch the leveling wizard from settings menu and follow on-screen directions until level is achieved.

![Flashforge Dreamer bed leveling](../../images/3d-printing/flashforge-dreamer-leveling.jpg)

Ensure the build plate is clean by scratching any print leftovers off the plate and wiping with IPA.

### Starting the print

Slice the file in a slicer appropriate for the printer. Save G-code to a SD card and insert the card into the printer. Select the model in menu on the printer, and start printing. The printer will preheat and start extruding material.

**Watch the printer until the first layer is done to prevent failure.**

### Removing the print

After a print has finished, let the build plate cool down before removing a print, especially if the contact surface between the print and build plate is large. For hard to remove prints, carefully use a spatula or blade.

## Creality Ender 3

![Creality Ender 3](../../images/3d-printing/creality-ender-3.jpg)

| Specifications          |               |
| ----------------------- | ------------- |
| Technology              | FDM / FFF     |
| Build volume            | 230x230x250mm |
| Layer height            | 0.1-0.3mm     |
| Maximum extrusion speed | 100mm/s       |
| Available materials     | PLA           |

### Slicer

For the Creality Ender 3 use the PrusaSlicer. You can download it <a href="https://www.prusa3d.com/prusaslicer/" target="_blank">from the official website</a>

![FlashPrint](../../images/3d-printing/prusaslicer.jpg)

After installing PrusaSlicer, The CORE printer configurations have to be installed. Download and run the configuration installer from <a href="../../../artifacts/configuration-installer.exe" download>this link</a>. The program will install the configuration file to:

`C:\Program Files\Prusa3D\PrusaSlicer\resources\profiles`

On first launch a configuration wizard will appear, where the available printers are to be selected. Select **The CORE** under **Other Vendors**. This will then create **The CORE FFF** submenu. Select the Ender 3 with 0.4mm nozzle from within the menu. Press **Finish**. If not selected by default, make sure to select **Creality ENDER-3 - The CORE** from the **Printer** dropdown menu in the right panel.

Should you have opened PrusaSlicer before installing the profiles, it will need to be restarted after installation. The configuration wizard can be launched from the **Configuration** drop-down in the top-left corner of the window.

### Before printing

Ender 3 printers in the CORE are equipped with BLtouch bed tramming probes. Using the probe together with a special G-code sequence on print startup eliminates the need to manually adjust the bed.

![Creality Ender 3 probe](../../images/3d-printing/creality-ender-3-probe.jpg)

Ensure the build plate is clean by scratching any print leftovers off the plate and wiping with IPA. Ensure the magnetic sheet is installed in a position that won't prevent the Y-axis from moving freely.

### Starting the print

Slice the file in a slicer appropriate for the printer. Save G-code to a SD card and insert the card into the printer. Keep in mind that the SD cards are inserted with the contacts up on the the Ender 3 printers. Select the model in menu on the printer, and start printing. On the Enders a mesh probing will be executed before printing starts. The printer will preheat and start extruding material.

**Watch the printer until the first layer is done to prevent failure.**

### Removing the print

The Ender 3 is equipped with a flexible removable bed sheet. Carefully lift the magnetic sheet off heater plate and twist it slightly to peel the print off.
